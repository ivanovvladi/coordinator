//
//  Coordinatable.swift
//  RxCoordinator
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import RxSwift

public protocol Coordinatable: Identifiable {
    
    var dependenciesManager: CoordinatorDependenciesManagerType { get }
    var disposeBag: DisposeBag { get }
    
    @discardableResult
    func coordinate(to coordinator: Coordinatable) -> Observable<Coordinatable>
    
    @discardableResult
    func start() -> Observable<Coordinatable>
    
}

open class Coordinator: Coordinatable {
    
    public let dependenciesManager: CoordinatorDependenciesManagerType
    public let disposeBag: DisposeBag

    public init(disposeBag: DisposeBag = DisposeBag(),
                dependenciesManager: CoordinatorDependenciesManagerType = CoordinatorDependenciesManager()) {
        
        self.disposeBag = disposeBag
        self.dependenciesManager = dependenciesManager
    }
    
    @discardableResult
    open func coordinate(to coordinator: Coordinatable) -> Observable<Coordinatable> {
        dependenciesManager.add(child: coordinator)
        
        return coordinator.start().do(onCompleted: { [weak self] in
            self?.dependenciesManager.remove(child: coordinator)
        })
    }
    
    @discardableResult
    open func start() -> Observable<Coordinatable> {
       fatalError("Start method should be overridden")
    }
    
}

