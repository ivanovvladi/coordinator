//
//  RxCoordinator.h
//  RxCoordinator
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Coordinator.
FOUNDATION_EXPORT double CoordinatorVersionNumber;

//! Project version string for Coordinator.
FOUNDATION_EXPORT const unsigned char CoordinatorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Coordinator/PublicHeader.h>
