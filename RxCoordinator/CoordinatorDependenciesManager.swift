//
//  CoordinatorDependenciesManager.swift
//  RxCoordinator
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol CoordinatorDependenciesManagerType: class {
    
    func add(child coordinator: Coordinatable)
    func remove(child coordinator: Coordinatable)
    func getDependencies() -> [Coordinatable]
    
}

open class CoordinatorDependenciesManager: CoordinatorDependenciesManagerType {
    
    private var dependenciesStorage = [String: Coordinatable]()
    
    public init() {
        
    }
    
    open func add(child coordinator: Coordinatable) {
        dependenciesStorage[coordinator.id] = coordinator
    }
    
    open func remove(child coordinator: Coordinatable) {
        dependenciesStorage[coordinator.id] = nil
    }
    
    open func getDependencies() -> [Coordinatable] {
        return dependenciesStorage.values.compactMap { $0 }
    }
    
}
