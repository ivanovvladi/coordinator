//
//  Identifiable.swift
//  RxCoordinator
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol Identifiable: class {
    
    var id: String { get }
    
}

extension Identifiable {
   
    public var id: String {
        return "\(type(of: self)):\(ObjectIdentifier(self))"
    }
    
}
