Pod::Spec.new do |spec|
  spec.name           = "RxCoordinatorTest"
  spec.version        = "0.1.1"
  spec.summary        = "Rx Coordinator Test"
  
  spec.description    = <<-DESC
    A simple test framework for the RxCoordinator
  DESC

  spec.homepage       = "https://gitlab.com/ivanovvladi"
  spec.license        = { :type => "Apache License Version 2.0", :file => "LICENSE" }
  spec.author         = { "Vladislav Ivanov" => "vladislav.ivanov.v@gmail.com" }
  spec.platform       = :ios, "10.0"
  spec.source         = { :git => "https://gitlab.com/ivanovvladi/Coordinator.git", :tag => "#{spec.version}" }
  spec.source_files   = "RxCoordinatorTest/**/*.{swift}"
  spec.swift_versions = "5.0"
  spec.dependency       "RxCoordinator", "#{spec.version}"
  spec.dependency       "RxSwift", "5.1.1"
  spec.dependency       "RxTest", "5.1.1"

end