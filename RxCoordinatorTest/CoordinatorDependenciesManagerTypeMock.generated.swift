// Generated using Sourcery 1.1.1 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Foundation
import RxCoordinator

open class CoordinatorDependenciesManagerTypeMock: CoordinatorDependenciesManagerType {

    public init() {
        
    }

    // MARK: - add

    public var addChildCallsCount = 0
    public var addChildCalled: Bool {
        return addChildCallsCount > 0
    }
    public var addChildReceivedCoordinator: Coordinatable?
    public var addChildReceivedInvocations: [Coordinatable] = []
    public var addChildClosure: ((Coordinatable) -> Void)?

    open func add(child coordinator: Coordinatable) {
        addChildCallsCount += 1
        addChildReceivedCoordinator = coordinator
        addChildReceivedInvocations.append(coordinator)
        addChildClosure?(coordinator)
    }

    // MARK: - remove

    public var removeChildCallsCount = 0
    public var removeChildCalled: Bool {
        return removeChildCallsCount > 0
    }
    public var removeChildReceivedCoordinator: Coordinatable?
    public var removeChildReceivedInvocations: [Coordinatable] = []
    public var removeChildClosure: ((Coordinatable) -> Void)?

    open func remove(child coordinator: Coordinatable) {
        removeChildCallsCount += 1
        removeChildReceivedCoordinator = coordinator
        removeChildReceivedInvocations.append(coordinator)
        removeChildClosure?(coordinator)
    }

    // MARK: - getDependencies

    public var getDependenciesCallsCount = 0
    public var getDependenciesCalled: Bool {
        return getDependenciesCallsCount > 0
    }
    public var getDependenciesReturnValue: [Coordinatable] = []
    public var getDependenciesClosure: (() -> [Coordinatable])?

    open func getDependencies() -> [Coordinatable] {
        getDependenciesCallsCount += 1
        return getDependenciesClosure.map({ $0() }) ?? getDependenciesReturnValue
    }

}
