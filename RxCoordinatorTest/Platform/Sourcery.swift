//
//  Sourcery.swift
//  RxCoordinatorTest
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import RxCoordinator

// sourcery: AutoMockable
extension Coordinatable {}

// sourcery: AutoMockable
extension CoordinatorDependenciesManagerType {}
