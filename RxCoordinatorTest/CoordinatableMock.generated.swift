// Generated using Sourcery 1.1.1 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Foundation
import RxCoordinator
import RxSwift

open class CoordinatableMock: Coordinatable {

    public init() {
        
    }
    
    public var dependenciesManager: CoordinatorDependenciesManagerType {
        get {
            return underlyingDependenciesManager
        }
        set {
            underlyingDependenciesManager = newValue
        }
    }
    public var underlyingDependenciesManager: CoordinatorDependenciesManagerType = CoordinatorDependenciesManagerTypeMock()


    public var disposeBag: DisposeBag {
        get { 
            return underlyingDisposeBag 
        }
        set { 
            underlyingDisposeBag = newValue 
        }
    }
    public var underlyingDisposeBag: DisposeBag = DisposeBag()

    public var id: String {
        get { 
            return underlyingId 
        }
        set { 
            underlyingId = newValue 
        }
    }
    public var underlyingId: String = ""

    // MARK: - coordinate

    public var coordinateToCallsCount = 0
    public var coordinateToCalled: Bool {
        return coordinateToCallsCount > 0
    }
    public var coordinateToReceivedCoordinator: Coordinatable?
    public var coordinateToReceivedInvocations: [Coordinatable] = []
    public var coordinateToReturnValue: Observable<Coordinatable> = PublishSubject<Coordinatable>().asObservable()
    public var coordinateToClosure: ((Coordinatable) -> Observable<Coordinatable>)?

    open func coordinate(to coordinator: Coordinatable) -> Observable<Coordinatable> {
        coordinateToCallsCount += 1
        coordinateToReceivedCoordinator = coordinator
        coordinateToReceivedInvocations.append(coordinator)
        return coordinateToClosure.map({ $0(coordinator) }) ?? coordinateToReturnValue
    }

    // MARK: - start

    public var startCallsCount = 0
    public var startCalled: Bool {
        return startCallsCount > 0
    }
    public var startReturnValue: Observable<Coordinatable> = PublishSubject<Coordinatable>().asObservable()
    public var startClosure: (() -> Observable<Coordinatable>)?

    open func start() -> Observable<Coordinatable> {
        startCallsCount += 1
        return startClosure.map({ $0() }) ?? startReturnValue
    }

}
