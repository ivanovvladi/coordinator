//
//  CoordinatorTests.swift
//  RxTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import XCTest
import RxCoordinator
import RxCoordinatorTest
import RxSwift
import RxTest

final class CoordinatorTests: XCTestCase {
    
    private let dependenciesManager = CoordinatorDependenciesManagerTypeMock()
    private let childCoordinator = CoordinatableMock()
    private let testScheduler = TestScheduler(initialClock: 0)
    private lazy var sut = Coordinator(dependenciesManager: dependenciesManager)
        
    func test_coordinate_calls_add_ofDependenciesManager() {
        let expected = true

        sut.coordinate(to: childCoordinator)
                
        XCTAssertEqual(expected, dependenciesManager.addChildCalled)
    }
    
    func test_coordinate_calls_start_ofChildCoordinator() {
        let expected = true
        
        sut.coordinate(to: childCoordinator)
        
        XCTAssertEqual(expected, childCoordinator.startCalled)
    }
    
    func test_coordinate_calls_remove_ofDependenciesManager_onFinishChildCoordinator() {
        let expected = true
        
        let subject = PublishSubject<Coordinatable>()
        childCoordinator.startReturnValue = subject.asObservable()
        
        let observable = sut.coordinate(to: childCoordinator)
        _ = subject.subscribe()
        subject.asObserver().onCompleted()
        
        _ = testScheduler.start {
            return observable
        }
                
        XCTAssertEqual(expected, dependenciesManager.removeChildCalled)
    }
    
    func test_coordinate_returns_observable_ofChildCoordinator() {
        let subject = PublishSubject<Coordinatable>()
        childCoordinator.startReturnValue = subject.asObservable()
        subject.asObserver().onCompleted()
        
        let expected = testScheduler.start {
            return subject.asObservable()
        }.events
        
        let observer = testScheduler.start {
            return self.sut.coordinate(to: self.childCoordinator)
        }
        
        XCTAssertEqual(expected.count, observer.events.count)
    }
    
}
