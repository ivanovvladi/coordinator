//
//  CoordinatorDependenciesManagerTests.swift
//  RxTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import XCTest
import RxCoordinator
import RxCoordinatorTest
    
final class CoordinatorDependenciesManagerTests: XCTestCase {
    
    private let coordinator = CoordinatableMock()
    private let sut = CoordinatorDependenciesManager()
    
    func test_add_retains_coordinator() {
        let expected = true
        
        sut.add(child: coordinator)
                
        XCTAssertEqual(expected, sut.getDependencies().contains { $0.id == coordinator.id })
    }
    
    func test_remove_releases_coordinator() {
        let expected = 0
        
        sut.add(child: coordinator)
        sut.remove(child: coordinator)
        
        XCTAssertEqual(expected, sut.getDependencies().count)
    }
    
}
